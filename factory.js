define(function (require) {
	'use strict';

	var Promise = require('promise');

	var rollingStorage = require('rolling-storage');

	return function (options) {

		var createKey = options.createKey;
		var createUrl = options.createUrl;
		var createData = options.createData;
		var timeout = options.timeout || 0;

		if (typeof createUrl !== 'function') {
			throw new Error('Services need a createUrl option. It should be a function that takes an identifier and returns a string.');
		}

		if (!createKey) {
			createKey = createUrl;
		}

		if (typeof createKey !== 'function') {
			throw new Error('A service was provided with a non-function for createKey.');
		}

		if (createData && typeof createData !== 'function') {
			throw new Error('If a service is provided with createData then it must be a function.');
		}

		if (typeof timeout !== 'number') {
			throw new Error('A service must be provided with a number for timeout, or you may default to not timing out.');
		}

		var cache;
		if (options.cache) {
			cache = rollingStorage(options.cache);
			timeout = 5000; // so many rapid calls don't hit localStorage.
		}

		var service = {};		
		var serviceController = {};

		var promises = {};
		var identifiers = {};
		var cacheClearTimeouts = {};

		if (options.mappings) {
			var mappings = Object.keys(options.mappings);
			mappings.forEach(function (mapping) {
				if (Promise.prototype[mapping]) {
					throw new Error('Illegal mapping: '+mapping+'. It conflicts with a native Promise function.');
				}
			});
		}

		function createPromise (resolver) {
			var promise = new Promise(resolver);

			if (options.mappings) {
				Object.keys(options.mappings).forEach(function (mapping) {
					promise[mapping] = function () {
						return promise.then(options.mappings[mapping]);
					};
					promise[mapping].original = promise;
				});
			}
			return promise;
		}

		function addToCache (identifier, promise) {
			var key = createKey(identifier);
			promises[key] = promise;
			identifiers[key] = identifier;

			if (timeout) {
				cacheClearTimeouts[identifier] = setTimeout(function () {
					clearCacheForIdentifier(identifier);
				}, timeout);
			}
		}

		function clearCacheForIdentifier (identifier, storageToo) {
			var key = createKey(identifier);
			clearTimeout(cacheClearTimeouts[key]);

			if (cache && storageToo) {
				cache.remove(key);
			}

			delete promises[key];
			delete(cacheClearTimeouts[key]);
			//emitter.trigger('clear', identifier);
		}

		function makeConnection (type, url, data, key) {
			var xhr = new XMLHttpRequest();
			xhr.withCredentials = true;

			var promise = createPromise(function(resolve, reject) {
				xhr.onreadystatechange = function () {
					if (xhr.readyState === 4) {
						if (xhr.status === 200) {
							var data = JSON.parse(xhr.responseText);
							if (key && cache) {
								cache.set(key, data);
							}
							resolve(data);
						} else {
							if (key) {
								delete promises[key];
							}

							if (options.errors && options.errors[xhr.status]) {
								return options.errors[xhr.status](xhr.responseText, xhr.status);
							}

							reject(JSON.parse(xhr.responseText));
						}
					}
				};
			});

			xhr.open(type, url, true);
			
			if (type !== 'GET') {
				var jsonString = JSON.stringify(data);
				xhr.setRequestHeader('content-type', 'application/json');				
				xhr.send(jsonString);
			} else {
				xhr.send();
			}

			return promise;
		}

		service.load = function (identifier) {

			if (identifier instanceof Promise) {
				return createPromise(function (resolve, reject) {
					identifier.then(function (resolvedIdentifier) {
						service.load(resolvedIdentifier).then(resolve, reject);
					}, reject);
				});
			}

			var key = createKey(identifier);

			if (promises[key]) {
				return promises[key];
			}

			if (cache && cache.has(key)) {
				return service.of(cache.get(key));
			}

			var url = createUrl(identifier);
			var promise;

			if (createData) {
				var data = createData(identifier);
				promise = makeConnection('POST', url, data, key);
			} else {
				promise = makeConnection('GET', url, undefined, key);
			}

			addToCache(identifier, promise);

			return promise.then(function (data) {
				if (onload) {
					onload(identifier, data);
				}
				return data;
			});

		};

		service.loadMany = function (identifiers) {
			var promises = identifiers.map(service.load);

			var promise = Promise.all(promises);

			if (options.mappings) {
				Object.keys(options.mappings).forEach(function (mapping) {
					promise[mapping] = function () {
						var mappedPromises = promises.map(function (promise) { return promise[mapping](); });
						return Promise.all(mappedPromises);
					};
					promise[mapping].original = promise;
				});
			}

			return promise;
		};

		service.put = function (data) {
			var url = createUrl();
			return makeConnection('PUT', url, data);
		};

		service.post = function (data) {
			var url = createUrl();
			return makeConnection('POST', url, data);
		};

		service.of = function (data) {
			return createPromise(function (resolve) {
				resolve(data);
			});
		};

		service.add = function (identifier, data) {
			
			var promise = createPromise(function (resolve) {
				resolve(data);
			});

			if (cache) {
				promise.then(function (data) {
					cache.set(createKey(identifier), data);
				});
			}

			addToCache(identifier, promise);
			
			return promise;
		};

		service.keyOf = createKey;

		serviceController.clear = function (identifier) {
			clearCacheForIdentifier(identifier, true);
		};

		serviceController.clearAll = function () {
			Object.keys(identifiers).forEach(function (key) {
				clearCacheForIdentifier(identifiers[key], true);
			});
		};

		serviceController.exportable = function () {
			return service;
		};

		return serviceController;
	};
});
