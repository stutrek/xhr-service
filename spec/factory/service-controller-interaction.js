var factory = require('../../factory');

describe('Service-Controller Interaction', function () {

	function makeBasicController () {
		var createUrl = function (id) { return 'hello '+id; };

		return factory({
			createUrl: createUrl
		});
	}

	var server;
	beforeEach(function () {
		server = sinon.fakeServer.create();
	});
	
	afterEach(function () { 
		server.restore();
	});

	it.skip('should throw a load event with the data it loaded', function (done) {
		var serviceController = makeBasicController();
		
		serviceController.on('load', function (data) {
			expect(data.dogfart).to.be(true);
			done();
		});

		var service = serviceController.exportable();

		service.load('world');

		server.requests[0].respond(
			200,
			{ "Content-Type": "application/json" },
			JSON.stringify({dogfart: true })
		);

	});
	
	it('should be able to clear the whole service cache', function (done) {
		var serviceController = makeBasicController();
		
		var service = serviceController.exportable();

		service.load('world').then(function () {
			serviceController.clearAll();
			service.load('world');
			expect(server.requests.length).to.be(2);
			done();
		});

		server.requests[0].respond(
			200,
			{ "Content-Type": "application/json" },
			JSON.stringify({dogfart: true })
		);
	});

	it('should be able to clear just one key', function (done) {
		var serviceController = makeBasicController();
		
		var service = serviceController.exportable();

		service.load('world');

		service.load('mars').then(function () {
			serviceController.clear('mars');
			service.load('world');
			service.load('mars');
			expect(server.requests.length).to.be(3);
			done();
		});

		server.requests[0].respond(
			200,
			{ "Content-Type": "application/json" },
			JSON.stringify({dogfart: true })
		);

		server.requests[1].respond(
			200,
			{ "Content-Type": "application/json" },
			JSON.stringify({astronautfart: true })
		);
	});

});
