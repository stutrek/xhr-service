var factory = require('../../factory');

describe('Service Factory', function () {

	it('should be a function', function () {
		expect(factory).to.be.a('function');
	});

	it('should make a controller', function () {
		var createUrl = function () { return 'hello'; };

		var serviceController = factory({
			createUrl: createUrl
		});

		expect(serviceController).to.be.an('object');
	});

	it('should error without createUrl', function () {
		expect(function () {
			factory({});
		}).to.throwError();
	});

	it('should error with a bad createKey', function () {
		var createUrl = function () { return 'hello'; };

		expect(function () {
			factory({
				createUrl: createUrl,
				createKey: 'plastic oversized'
			});
		}).to.throwError();
	});

	it('should error with a bad createData', function () {
		var createUrl = function () { return 'hello'; };

		expect(function () {
			factory({
				createUrl: createUrl,
				createData: 'android'
			});
		}).to.throwError();
	});
});
