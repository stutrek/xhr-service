var factory = require('../../factory');
var Promise = require('promise');

describe('Service Objects', function () {

	var server;
	beforeEach(function () {
		server = sinon.fakeServer.create();
	});
	
	afterEach(function () { 
		server.restore();
	});

	it('should have the right interface', function () {
		var service = factory({
			createUrl: function () { return 'hello'; }
		}).exportable();
		
		expect(service.load).to.be.a('function');
		expect(service.loadMany).to.be.a('function');
		expect(service.of).to.be.a('function');
		expect(service.add).to.be.a('function');
		expect(service.keyOf).to.be.a('function');
	});

	it('should create keys with the supplied method', function () {

		var service = factory({
			createUrl: function (id) { return 'hello ' + id; }
		}).exportable();

		var id = 'world';
		expect(service.keyOf(id)).to.be('hello world');

	});

	it('should make a GET to the url provided by createUrl', function () {

		var service = factory({
			createUrl: function (id) { return 'hello/' + id; }
		}).exportable();

		service.load('world');

		expect(server.requests[0].url).to.be('hello/world');
		expect(server.requests[0].method).to.be('GET');
	});
	
	it('should return data from the server', function (done) {

		var service = factory({
			createUrl: function (id) { return 'hello/' + id; }
		}).exportable();

		service.load('world').then(function (data) {
			console.log(data);
			expect(data.dogfart).to.equal(true);
			done();
		});

		server.requests[0].respond(
			200,
			{ "Content-Type": "application/json" },
			JSON.stringify({dogfart: true })
		);
	});
	
	it('should reject on a server error and provide XHR', function (done) {

		var service = factory({
			createUrl: function (id) { return 'hello/' + id; }
		}).exportable();

		service.load('world').then(function () {
			throw new Error('resolved when it should not have been');
		}, function (xhr) {
			expect(xhr.status).to.be(500);
			done();
		});

		server.requests[0].respond(
			500,
			{ "Content-Type": "application/json" },
			JSON.stringify({dogfart: true })
		);
	});

	it('should cache a successful attempt', function (done) {

		var service = factory({
			createUrl: function (id) { return 'hello/' + id; }
		}).exportable();

		service.load('world').then(function () {
			service.load('world');
			expect(server.requests).to.have.length(1);
			done();
		});

		server.requests[0].respond(
			200,
			{ "Content-Type": "application/json" },
			JSON.stringify({dogfart: true })
		);
	});
	
	it('should not cache an error', function (done) {

		var service = factory({
			createUrl: function (id) { return 'hello/' + id; }
		}).exportable();

		service.load('world').then(function () {
			throw new Error('resolved when it should not have been');
		}, function (xhr) {
			service.load('world');
			expect(server.requests).to.have.length(2);
			done();
		});

		server.requests[0].respond(
			500,
			{ "Content-Type": "application/json" },
			JSON.stringify({dogfart: true })
		);
	});

	it('should add an item to the cache', function () {

		var service = factory({
			createUrl: function (id) { return 'hello/' + id; }
		}).exportable();

		service.add('world', {dogfart: true});
		service.load('world');

		expect(server.requests).to.have.length(0);
	});

	it('should expire items from the cache', function () {

		var clock = sinon.useFakeTimers();

		var service = factory({
			createUrl: function (id) { return 'hello/' + id; },
			timeout: 4
		}).exportable();

		service.load('world');

		server.requests[0].respond(
			200,
			{ "Content-Type": "application/json" },
			JSON.stringify({dogfart: true })
		);

		service.load('world');
		expect(server.requests.length).to.be(1);

		clock.tick(5);

		service.load('world');
		expect(server.requests.length).to.be(2);

		clock.restore();

	});

	it('should create a promise if you already have data', function (done) {

		var service = factory({
			createUrl: function (id) { return 'hello/' + id; }
		}).exportable();

		var dogfart = service.of({dogfart: true});

		expect(dogfart).to.be.a(Promise);

		dogfart.then(function (data) {
			expect(data.dogfart).to.be(true);
			done();
		});
	});

	it('should make multiple calls when loading many', function (done) {

		var service = factory({
			createUrl: function (id) { return 'hello/' + id; }
		}).exportable();

		var many = service.loadMany(['earth', 'mars']).then(function (datas) {
			expect(datas[0].dogfart).to.equal(true);
			expect(datas[1].astronautfart).to.equal(true);
			done();
		});

		expect(server.requests.length).to.be(2);

		server.requests[0].respond(
			200,
			{ "Content-Type": "application/json" },
			JSON.stringify({dogfart: true })
		);

		server.requests[1].respond(
			200,
			{ "Content-Type": "application/json" },
			JSON.stringify({astronautfart: true })
		);

	}); 

});
