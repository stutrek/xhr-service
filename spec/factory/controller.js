var factory = require('../../factory');

describe('Service Controllers', function () {

	function makeBasicController () {
		var createUrl = function () { return 'hello'; };

		return factory({
			createUrl: createUrl
		});
	}

	it.skip('should be an emitter', function () {
		var serviceController = makeBasicController();
		
		expect(serviceController.on).to.be.a('function');
		expect(serviceController.off).to.be.a('function');
		expect(serviceController.once).to.be.a('function');
		expect(serviceController.trigger).to.be.a('function');
	});
	
	it('should have clearing methods', function () {
		var serviceController = makeBasicController();
		
		expect(serviceController.clear).to.be.a('function');
		expect(serviceController.clearAll).to.be.a('function');
	});

	it('should make one and only one exportable', function () {
		var serviceController = makeBasicController();

		var service = serviceController.exportable();
		var service2 = serviceController.exportable();
		
		expect(service).to.be.an('object');
		expect(service).to.be(service2);
	});
});
