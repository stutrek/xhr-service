
if(!Function.prototype.bind){
	Function.prototype.bind = function(){
		[].unshift.call(arguments, this);
		return $.proxy.apply($,arguments);
	};
}


require('./factory/factory');
require('./factory/controller');
require('./factory/service');
require('./factory/service-controller-interaction');
