define(function (require) {
	'use strict';
	var _ = require('underscore');
	var Backbone = require('backbone');

	function mixin (options) {

		var service = options.service;

		this.clobber({
			sync: function (method, model, options) {
				var methodMap = {
					'create': 'POST',
					'update': 'PUT',
					'patch': 'PATCH',
					'delete': 'DELETE',
					'read': 'GET'
				};
				var urlError = function() {
					throw new Error('A "url" property or function must be specified');
				};

				///// start


				var type = methodMap[method];

				// Default options, unless specified.
				_.defaults(options || (options = {}), {
					emulateHTTP: Backbone.emulateHTTP,
					emulateJSON: Backbone.emulateJSON
				});

				// Default JSON-request options.
				var params = {
					type: type,
					dataType: 'json'
				};

				// Ensure that we have a URL.
				if (!options.url) {
					params.url = _.result(model, 'url') || urlError();
				}

				// Ensure that we have the appropriate request data.
				if (options.data == null && model && (method === 'create' || method === 'update' || method === 'patch')) {
					params.contentType = 'application/json';
					params.data = JSON.stringify(options.attrs || model.toJSON(options));
				}

				// Don't process data on a non-GET request.
				if (params.type !== 'GET' && !options.emulateJSON) {
					params.processData = false;
				}

				// make service call
				var promise = service.load(this.id);

				// wrap in jqXHR like object
				var dfd = $.Deferred();
				var xhr = dfd.promise();

				if (options.success) {
					promise.then(options.success.bind(this));
				}
				if (options.failure) {
					promise.then(undefined, options.failure.bind(this));
				}

				promise.then(function (data) {
					dfd.resolve(data);
				},function (err) {
					dfd.reject(err);
				});

				// Make the request, allowing the user to override any Ajax options.
				//var xhr = options.xhr = Backbone.ajax(_.extend(params, options));
				model.trigger('request', model, xhr, options);
				return xhr;
			}
		});
	}
	return mixin;
});
